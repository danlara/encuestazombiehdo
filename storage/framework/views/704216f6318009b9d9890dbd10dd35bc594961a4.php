
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo e(asset ('../../favicon.ico')); ?>">

    <title>.: <?php echo $__env->yieldContent('title'); ?> | Wolking Dead - Encuenta:.</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo e(asset ('css/normalize.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset ('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset ('css/style.css')); ?>">
  </head>
  <body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          </button>
          <a class="navbar-brand .col-xs-2" href="/"><img src="<?php echo e(asset ('img/hdo.png')); ?>" alt=""></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          </ul>
          <ul class="nav navbar-nav navbar-right .col-xs-10">
            <li><a href="/"><span class="glyphicon glyphicon-home"></span> Inicio</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <?php echo $__env->yieldContent('content'); ?>
          <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->


    <script src="<?php echo e(asset ('js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset ('js/bootstrap.min.js')); ?>"></script>
    <scrip src="<?php echo e(asset ('js/validator.js')); ?>"></script>
  <!--  <script src="js/xepOnline.jpPlugin.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo e(asset ('js/jquery-3.2.0.min.js')); ?>"><\/script>')</script> -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <footer>
      <div class="container">
       <div class="row">
         <div class="col-xs-12 col-lg-12">
           <br>
            HDO-COMPANY - Derechos Reservados 2017
            <br>
         </div>
       </div>
       <div class="row">
         <div class="col-xs-12 col-lg-12">
           <br>
           <br>
         </div>
       </div>
      </div>
    </footer>
  </body>
</html>
