
<?php $__env->startSection('title'); ?>
  Registro
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
  <br>
  <div id="fomulary" class="container">
    <div id="personal"  class="jumbotron">
    <?php echo Form::open(['url' => '/user', 'method' => 'post']); ?>

    <div class="form-group">
      <?php echo Form::label('name', 'Nombre:'); ?>

      <?php echo Form::text('name', null, ['class'=> 'form-control', 'placeholder' =>'Nombre Completo', 'required','maxlength' => 80]); ?>

    </div>
    <div class="form-group">
      <?php echo Form::label('las_name', 'Apellidos:'); ?>

      <?php echo Form::text('last_name', null, ['class'=> 'form-control', 'placeholder' =>'Lopez', 'required', 'maxlength' => 50]); ?>

    </div>
    <div class="form-group">
      <?php echo Form::label('email', 'Correo Electronico:'); ?>

      <?php echo Form::email('email', null, ['class'=> 'form-control', 'placeholder' =>'example@org.com', 'required','maxlength' => 80]); ?>

    </div>
    <div class="form-group">
      <?php echo Form::label('phone', 'Telefono:'); ?>

      <?php echo Form::text('phone', null, ['class' => 'form-control','placeholder' =>'015512345678','maxlength' =>20]); ?>

    </div>
    <div class="form-group">
      <?php echo Form::label('company', 'Empresa:'); ?>

      <?php echo Form::text('company', null, ['class'=> 'form-control', 'placeholder' =>'Empresa o Compañia', 'required', 'maxlength' => 50]); ?>

    </div>
    <div class="form-group">
      <?php echo Form::label('job', 'Puesto:'); ?>

      <div class="radio">
        <label><input type="radio" name="job" value="Jefe" required="requiered">Jefe</label>
        <label><input type="radio" name="job" value="Subordinado" required="requiered">Subordinado</label>
      </div>
    </div>
    <div class="form-group">
      <?php echo Form::label('country', 'Pais:'); ?>

      <select class="form-control" >
      <?php $__currentLoopData = $paises; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pais): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($pais->name); ?>"><?php echo e($pais->name); ?></option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </select>
    </div>
    <div class="form-group">
    <?php echo e(Form::hidden('active', '1')); ?>

    <?php echo e(Form::hidden('type', 'visit')); ?>

    </div>
    <div class="form-group">
      <?php echo Form::submit('Registro', ['class'=>'btn btn-success']); ?>

    </div>
    <?php echo Form::close(); ?>

  </div>
</div>
</div>
</form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>