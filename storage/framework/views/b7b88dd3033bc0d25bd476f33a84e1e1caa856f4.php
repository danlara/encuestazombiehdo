
<?php $__env->startSection('title'); ?>
  Test Zombie
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
  <div class="container">
    <div id="personal" class="jumbotron">
      <?php echo Form::open(['url' => '/testend', 'method' => 'post']); ?>

      <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php $contador = $contador+1 ?>
      <div class="form-group">
        <?php echo e($contador); ?> .- <label for="<?php echo e($question->id); ?>"><?php echo e($question->name); ?></label>
        <?php $__currentLoopData = $answers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $answer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php if($question->id == $answer->id_questions): ?>
          <div class="radio">
            <label id="resp"><input type="radio" name="<?php echo e($question->id); ?>" value="<?php echo e($answer->id); ?>" required="requiered"><p id="resps"><?php echo e($answer->name); ?></p></label>
          </div>
          <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <div class="form-group">
        <?php echo e(Form::hidden('active', '1')); ?>

        <?php echo e($questions->links()); ?>

      </div>
      <div class="form-group">
        <?php echo Form::submit('Finalizar Encuesta', ['class'=>'btn btn-primary']); ?>

      </div>
      <?php echo Form::close(); ?>

    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>