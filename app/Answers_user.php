<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers_user extends Model
{
    //
    protected $table = "answers_users";

    protected $fillable = ['id_users','id_answers','id_questions','id_count_questionary','active'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function answers()
    {
      return $this->belongsToMany('App\Answer')->withTimestamps();
    }

    public function question()
    {
      return $this->belongsTo('App\Question');
    }


    public function count_questionary()
    {
      return $this->belongsTo('App\Count_questionary');
    }

}
