<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Count_questionary extends Model
{
    //
    protected $table = "count_questionary";

    protected $fillable = ['id_users'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function answers_users()
    {
    return $this->hasMany('App\answers_users');
    }

    public function report_users()
    {
    return $this->hasMany('App\Report_user');
  }
}
