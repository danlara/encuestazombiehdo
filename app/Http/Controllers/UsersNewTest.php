<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Question;
use App\Answer;
use App\Answers_user;
use App\Count_questionary;
use App\Zombie;
use App\Report_user;
use App\Pais;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;
use SnappyImage;

$zombieCastrador;
$zombieCambio;
$zombieDesmotivador;
$zombieChismoso; $zombieNegativo;
$styleCastrador;
$styleCambio;
$styleDesmotivador;
$styleChismoso;
$styleNegativo;
$user;

class UsersNewTest extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function intro()
     {
       global $paises;
       $paises = DB::table('paises')->get();
       return View('nametest',['paises'=>$paises]);
     }

     public function createuser(Request $request){
       //dd($request->all());
       global $user;
       $user = new User($request->all());
       $user->save();
       $request->session()->put('user', $user);

       $intento = new Count_questionary();
       $intento->id_users = $user->id;
       $intento->save();
       $request->session()->put('intento', $intento);

       //return "Hola";
     return redirect()->action('UsersNewTest@index');

     }

    public function index()
    {
      //$valor = Question::all();
      //$valor->each(function($valor){});
      //dd($valor);

      $questions = Question::orderBy('id','ASC')->paginate(150);
      Log::info($questions);
      //$answers = Answer::orderBy('id','ASC');
      $answers = DB::table('answers')->get();
      $contador=0;

      //return view('testzombie')->with('questions', $questions, 'answers', $answers);
      return View('testzombie',['questions'=> $questions, 'answers'=> $answers, 'contador'=> $contador ]);

      //dd([$questions->toArray()]);


        //$valor->question;
        //$valor->zombies;

      //dd([$valor->toArray()]);
      //
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      global $zombieCastrador, $zombieCambio, $zombieDesmotivador, $zombieChismoso, $zombieNegativo,$styleCastrador,
      $styleCambio,$styleDesmotivador,$styleChismoso, $styleNegativo, $user;
        //
        //dd($request);
        //Log::warning($request->all());
        //dd($request->all());
        $user = $request->session()->get('user');
        $intento = $request->session()->get('intento');
        //dd($user->id);
        //dd($request->session()->all());
        //dd($request->all());
        $rest = array();
      foreach ($request->all() as $id_preg => $id_resp) {
          if(!is_int($id_preg)) continue;
          $respuesta = new Answers_user();
          $respuesta->id_users =$user->id;
          $respuesta->id_answers = $id_resp;
          $respuesta->id_questions = $id_preg;
          $respuesta->id_count_questionary = $intento->id;
          $respuesta->active = $request->input('active');
          $respuesta->save();
          Log::info($respuesta);

          $reporte = DB::table('answers')
          ->select('id_zombie', 'value')
          ->where('id', $id_resp)
          ->first();
          array_push($rest,$reporte);
      }

          //$reporte = Answer::where('id',$id_resp)->first();
          //$zombie = Zombie::where('id',$reporte->id_zombie)->first();
          //var_dump($zombie);
        //  array_push($rest,$zombie);
          # code...

        //dd($rest);
        // echo $variable['indice_array']

//Zombie Jefe Castrador

        $zombie5 = 0;
        foreach ($rest as $respuestas5 => $calificacion5) {
             foreach ($calificacion5 as $valor5) {
              if($calificacion5->id_zombie == 5) {
                $zombie5 += ($calificacion5->value)/2;
              }
             }
        }
//Zombie Resistente al Cambio
        $zombie6 = 0;
        foreach ($rest as $respuestas6 => $calificacion6) {
             foreach ($calificacion6 as $valor6) {
              if($calificacion6->id_zombie == 6) {
                $zombie6 += ($calificacion6->value)/2;
              }
             }
        }
//Zombie Desmotivador
          $zombie7 = 0;
          foreach ($rest as $respuestas7 => $calificacion7) {
              foreach ($calificacion7 as $valor7) {
              if($calificacion7->id_zombie == 7) {
                $zombie7 += ($calificacion7->value)/2;
              }
              }
          }
//Zombie Chismoso
          $zombie8 = 0;
          foreach ($rest as $respuestas8 => $calificacion8) {
              foreach ($calificacion8 as $valor8) {
              if($calificacion8->id_zombie == 8) {
                $zombie8 +=( $calificacion8->value)/2;
              }
              }
          }

//Zombie negativo
          $zombie9 = 0;
          foreach ($rest as $respuestas9 => $calificacion9) {
              foreach ($calificacion9 as $valor9) {
              if($calificacion9->id_zombie == 9) {
                $zombie9 +=( $calificacion9->value)/2;
              }
              }
          }

        $zombieCastrador = ($zombie5* 100)/16;
        $zombieCambio = ($zombie6* 100)/16;
        $zombieDesmotivador = ($zombie7* 100)/16;
        $zombieChismoso = ($zombie8* 100)/16;
        $zombieNegativo = ($zombie9 * 100)/16;

        $z5 = new Report_user();
        $z5->id_count_questionary = $intento->id;
        $z5->id_users =$user->id;
        $z5->id_zombie= 5;
        $z5->name = $zombieCastrador;
        $z5->active = $request->input('active');
        $z5->save();

        $z6 = new Report_user();
        $z6->id_count_questionary = $intento->id;
        $z6->id_users =$user->id;
        $z6->id_zombie= 6;
        $z6->name = $zombieCambio;
        $z6->active = $request->input('active');
        $z6->save();

        $z7 = new Report_user();
        $z7->id_count_questionary = $intento->id;
        $z7->id_users =$user->id;
        $z7->id_zombie= 7;
        $z7->name = $zombieDesmotivador;
        $z7->active = $request->input('active');
        $z7->save();

        $z8 = new Report_user();
        $z8->id_count_questionary = $intento->id;
        $z8->id_users =$user->id;
        $z8->id_zombie= 8;
        $z8->name = $zombieChismoso;
        $z8->active = $request->input('active');
        $z8->save();

        $z9 = new Report_user();
        $z9->id_count_questionary = $intento->id;
        $z9->id_users =$user->id;
        $z9->id_zombie= 9;
        $z9->name = $zombieNegativo;
        $z9->active = $request->input('active');
        $z9->save();


       /* Comparadores de porcentajes */
      if ($zombieCastrador >=0 && $zombieCastrador <25)
       {
       $styleCastrador = "castrador_0";
       }
      elseif ($zombieCastrador >=25 && $zombieCastrador <50) {
       $styleCastrador = "castrador_25";
       }
      elseif ($zombieCastrador >=50 && $zombieCastrador <75) {
       $styleCastrador = "castrador_50";
       }
      elseif ($zombieCastrador >=75 && $zombieCastrador <100) {
       $styleCastrador = "castrador_75";
       }
      elseif ($zombieCastrador == 100) {
       $styleCastrador = "castrador_100";
        }

      if ($zombieCambio >=0 && $zombieCambio <25)
       {
       $styleCambio = "resistente_0";
       }
      elseif ($zombieCambio >=25 && $zombieCambio <50) {
       $styleCambio = "resistente_25";
       }
      elseif ($zombieCambio >=50 && $zombieCambio <75) {
       $styleCambio = "resistente_50";
       }
       elseif ($zombieCambio >=75 && $zombieCambio <100) {
        $styleCambio = "resistente_75";
       }
        elseif ($zombieCambio == '100') {
         $styleCambio = "resistente_100";
       }

      if ($zombieDesmotivador >=0 && $zombieDesmotivador <25)
       {
        $styleDesmotivador = "desmotivador_0";
       }
      elseif ($zombieDesmotivador >=25 && $zombieDesmotivador <50) {
        $styleDesmotivador = "desmotivador_25";
       }
      elseif ($zombieDesmotivador >=50 && $zombieDesmotivador <75) {
        $styleDesmotivador = "desmotivador_50";
       }
      elseif ($zombieDesmotivador >=75 && $zombieDesmotivador <100) {
        $styleDesmotivador = "desmotivador_75";
       }
      elseif ($zombieDesmotivador == 100) {
        $styleDesmotivador = "desmotivador_100";
       }

       if ($zombieChismoso >=0 && $zombieChismoso <25)
        {
         $styleChismoso = "chismoso_0";
        }
       elseif ($zombieChismoso >=25 && $zombieChismoso <50) {
         $styleChismoso = "chismoso_25";
        }
       elseif ($zombieChismoso >=50 && $zombieChismoso <75) {
         $styleChismoso = "chismoso_50";
        }
       elseif ($zombieChismoso >=75 && $zombieChismoso <100) {
         $styleChismoso = "chismoso_75";
        }
       elseif ($zombieChismoso == 100) {
         $styleChismoso = "chismoso_100";
        }

        if ($zombieNegativo >=0 && $zombieNegativo <25)
         {
          $styleNegativo = "negativo_0";
         }
        elseif ($zombieNegativo >=25 && $zombieNegativo <50) {
          $styleNegativo = "negativo_25";
         }
        elseif ($zombieNegativo >=50 && $zombieNegativo <75) {
          $styleNegativo = "negativo_50";
         }
        elseif ($zombieNegativo >=75 && $zombieNegativo <100) {
          $styleNegativo = "negativo_75";
         }
        elseif ($zombieNegativo == 100) {
          $styleNegativo = "negativo_100";
         }
  //return redirect()->action('PdfController@pdf');
  return View('report_test',
  ['zcastrador'=> $zombieCastrador,
   'zcambio' => $zombieCambio,
   'zdesmotivador' => $zombieDesmotivador,
   'zchismoso' => $zombieChismoso,
   'znegativo' => $zombieNegativo,
   'styleCastrador' =>$styleCastrador,
   'styleCambio' =>$styleCambio,
   'styleDesmotivador' =>$styleDesmotivador,
   'styleChismoso' =>$styleChismoso,
   'styleNegativo' =>$styleNegativo,
   'usuario' => $user]);


    //  dd($rest);
    }

    public function showpdf(Request $request)
    {

    $name = $request->input('name');
    $company = $request->input('company');
    $job = $request->input('job');
    $styleCastrador = $request->input('styleCastrador');
    $zcastrador = $request->input('zcastrador');
    $styleDesmotivador = $request->input('styleDesmotivador');
    $zdesmotivador = $request->input('zdesmotivador');
    $styleCambio = $request->input('styleCambio');
    $zcambio = $request->input('zcambio');
    $styleNegativo = $request->input('styleNegativo');
    $znegativo = $request->input('znegativo');
    $styleChismoso = $request->input('styleChismoso');
    $zchismoso = $request->input('zchismoso');
/*
    return View('report_pdf',
    ['name'=> $name,
    'company'=> $company,
    'job'=> $job,
    'styleCastrador'=> $styleCastrador,
    'zcastrador'=> $zcastrador,
    'styleDesmotivador'=> $styleDesmotivador,
    'zdesmotivador'=> $zdesmotivador,
    'styleCambio'=> $styleCambio,
    'zcambio'=> $zcambio,
    'styleNegativo'=> $styleNegativo,
    'znegativo'=> $znegativo,
    'styleChismoso'=> $styleChismoso,
    'zchismoso'=> $zchismoso]);
 */
    $pdf = \App::make('snappy.pdf.wrapper');
    $pdf = PDF::loadView('report_pdf',
    ['name'=> $name,
    'company'=> $company,
    'job'=> $job,
    'styleCastrador'=> $styleCastrador,
    'zcastrador'=> $zcastrador,
    'styleDesmotivador'=> $styleDesmotivador,
    'zdesmotivador'=> $zdesmotivador,
    'styleCambio'=> $styleCambio,
    'zcambio'=> $zcambio,
    'styleNegativo'=> $styleNegativo,
    'znegativo'=> $znegativo,
    'styleChismoso'=> $styleChismoso,
    'zchismoso'=> $zchismoso]);
    $pdf->setPaper('a4');
    $pdf->setOrientation('landscape');
    $pdf->setOption('margin-bottom', 0);
  //  return $pdf->stream('Reporte_'.$name.'.pdf');
    return $pdf->download('Reporte_'.$name.'.pdf');
    //$pdf = \PDF::loadFile('http://www.ine.mx/');
    //$pdf->stream('github.pdf');
    //dd($pdf);
  }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
