<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zombie extends Model
{
    //
    protected $table = "zombie";

    protected $fillable = ['name','active'];


    public function answers()
    {
    return $this->hasMany('App/Answer');
    }

    public function report_users()
    {
      return $this->belongsTo('App/Report_user');
    }

}
