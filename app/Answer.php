<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $table = "answers";

    protected $fillable = ['id_questions','id_zombie','name','active','value'];

    public function question()
    {
      return $this->belongsTo('App\Question');
    }

    public function zombie()
    {
      return $this->belongsTo('App\Zombie');
    }

    public function answers_users()
    {
      return $this->belongsToMany('App\Answers_user')->withTimestamps();
    }


}
