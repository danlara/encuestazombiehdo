<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_question extends Model
{
    //
    protected $table = "type_questions";

    protected $fillable = ['name','active'];

    public function questions()
    {
    return $this->hasMany('App\Question');
    }
}
