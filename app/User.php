<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','last_name','phone', 'email', 'country', 'password','job','company','active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function count_questionarys()
    {
    return $this->hasMany('App\Count_questionary');

    }

    public function answers_users()
    {
    return $this->hasMany('App\Answers_user');

    }

    public function report_users()
    {
    return $this->hasMany('App\Report_user');

    }




}
