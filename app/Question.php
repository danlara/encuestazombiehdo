<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $table = "questions";

    protected $fillable = ['name','id_type_questios','active'];

    public function answers()
    {
    return $this->hasMany('App\Answer');

    }

    public function answers_users()
    {
    return $this->hasMany('App\Answers_users');
    }


    public function type_questions()
    {
      return $this->belongsTo('App\Type_question');
    }


}
