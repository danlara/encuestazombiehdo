<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report_user extends Model
{
    //
    protected $table = "report_users";

    protected $fillable = ['name','active','id_count_questionary','id_users','id_zombie'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function count_questionary()
    {
      return $this->belongsTo('App\Count_questionary');
    }

    public function zombies()
    {
    return $this->hasMany('App\Zombie');
    }
}
