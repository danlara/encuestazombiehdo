<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('phone')->nullable();
            $table->string('email');
            $table->string('country');
            $table->string('company');
            $table->string('job');
            $table->string('password');
            $table->integer('active');
            $table->enum('type',['visit','admin','member'])->default('visit');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
        Schema::create('type_questions', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->integer('active');
            $table->timestamps();
        });
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->integer('id_type_questios')->unsigned();
            $table->foreign('id_type_questios')->references('id')->on('type_questions')->onDelete('cascade');
            $table->integer('active');
            $table->timestamps();
          });

          Schema::create('zombie', function (Blueprint $table) {
              $table->increments('id')->unique();
              $table->string('name');
              $table->integer('active');
              $table->timestamps();
          });

          Schema::create('answers', function (Blueprint $table) {
              $table->increments('id')->unique();
              $table->string('name');
              $table->integer('id_questions')->unsigned();
              $table->integer('id_zombie')->unsigned();
              $table->foreign('id_questions')->references('id')->on('questions')->onDelete('cascade');
              $table->foreign('id_zombie')->references('id')->on('zombie')->onDelete('cascade');
              $table->integer('active');
              $table->integer('value');

              $table->timestamps();
          });
          Schema::create('count_questionary', function (Blueprint $table) {
              $table->increments('id')->unique();
              $table->integer('id_users')->unsigned();
              $table->foreign('id_users')->references('id')->on('users')->onDelete('cascade');
              $table->timestamps();
          });

          Schema::create('answers_users', function (Blueprint $table) {
            /* $table->integer('user_id')->unsigned(); */
            /*  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); */
              $table->increments('id')->unique();
              $table->integer('id_users')->unsigned();
              $table->integer('id_answers')->unsigned();
              $table->integer('id_questions')->unsigned();
              $table->integer('id_count_questionary')->unsigned();
              $table->foreign('id_users')->references('id')->on('users')->onDelete('cascade');
              $table->foreign('id_answers')->references('id')->on('answers')->onDelete('cascade');
              $table->foreign('id_questions')->references('id')->on('questions')->onDelete('cascade');
              $table->foreign('id_count_questionary')->references('id')->on('count_questionary')->onDelete('cascade');
              $table->integer('active');
              $table->timestamps();
          });
          Schema::create('report_users', function (Blueprint $table) {
            /* $table->integer('user_id')->unsigned(); */
            /*  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); */
              $table->increments('id')->unique();
              $table->integer('id_count_questionary')->unsigned();
              $table->integer('id_users')->unsigned();
              $table->integer('id_zombie')->unsigned();
              $table->foreign('id_count_questionary')->references('id')->on('count_questionary')->onDelete('cascade');
              $table->foreign('id_users')->references('id')->on('users')->onDelete('cascade');
              $table->foreign('id_zombie')->references('id')->on('zombie')->onDelete('cascade');
              $table->string('name');
              $table->integer('active');
              $table->timestamps();
          });
          Schema::create('paises', function (Blueprint $table) {
            /* $table->integer('user_id')->unsigned(); */
            /*  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); */
              $table->increments('id')->unique();
              $table->string('abreviature');
              $table->string('name');
              $table->integer('active');
              $table->timestamps();
          });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
      /*  Schema::dropIfExists('count_questionary');
        Schema::dropIfExists('report_users');
        Schema::dropIfExists('answers_users');
        Schema::dropIfExists('zombie');
        Schema::dropIfExists('answers');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('type_questions'); */
    }
}
