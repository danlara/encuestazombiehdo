<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
/*
Route::get('/', function(){
  return View('nametest');
});
*/

Route::get('/', 'UsersNewTest@intro');

//Route::get('test/{id}',  'UsersNewTest@index');
Route::post('/user','UsersNewTest@createuser');

Route::get('/test','UsersNewTest@index');
Route::post('/testend','UsersNewTest@store');
Route::post('/pdf','UsersNewTest@showpdf');
