@extends('layouts.principal')
@section('title')
  Registro
@endsection
@section('content')
  <br>
  <div id="fomulary" class="container">
    <div id="personal"  class="jumbotron">
    {!! Form::open(['url' => '/user', 'method' => 'post']) !!}
    <div class="form-group">
      {!! Form::label('name', 'Nombre:')!!}
      {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder' =>'Nombre Completo', 'required','maxlength' => 80])!!}
    </div>
    <div class="form-group">
      {!! Form::label('las_name', 'Apellidos:')!!}
      {!! Form::text('last_name', null, ['class'=> 'form-control', 'placeholder' =>'Lopez', 'required', 'maxlength' => 50])!!}
    </div>
    <div class="form-group">
      {!! Form::label('email', 'Correo Electronico:')!!}
      {!! Form::email('email', null, ['class'=> 'form-control', 'placeholder' =>'example@org.com', 'required','maxlength' => 80])!!}
    </div>
    <div class="form-group">
      {!! Form::label('phone', 'Telefono:')!!}
      {!! Form::text('phone', null, ['class' => 'form-control','placeholder' =>'015512345678','maxlength' =>20]) !!}
    </div>
    <div class="form-group">
      {!! Form::label('company', 'Empresa:')!!}
      {!! Form::text('company', null, ['class'=> 'form-control', 'placeholder' =>'Empresa o Compañia', 'required', 'maxlength' => 50])!!}
    </div>
    <div class="form-group">
      {!! Form::label('job', 'Puesto:')!!}
      <div class="radio">
        <label><input type="radio" name="job" value="Jefe" required="requiered">Jefe</label>
        <label><input type="radio" name="job" value="Subordinado" required="requiered">Subordinado</label>
      </div>
    </div>
    <div class="form-group">
      {!! Form::label('country', 'Pais:')!!}
      <select class="form-control" >
      @foreach ($paises as $pais)
        <option value="{{$pais->name}}">{{$pais->name}}</option>
      @endforeach
      </select>
    </div>
    <div class="form-group">
    {{ Form::hidden('active', '1') }}
    {{ Form::hidden('type', 'visit') }}
    </div>
    <div class="form-group">
      {!! Form::submit('Registro', ['class'=>'btn btn-success'])!!}
    </div>
    {!! Form::close()!!}
  </div>
</div>
</div>
</form>
@endsection
