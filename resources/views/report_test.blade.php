@extends('layouts.principal')
@section('title')
  Resultados
@endsection
@section('content')
<?php
$zcastrador= round($zcastrador);
$zdesmotivador = round($zdesmotivador);
$zcambio = round($zcambio);
$znegativo = round($znegativo);
$zchismoso = round($zchismoso);
?>
<br><br><br>
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-4 col-md-4">
      <p class="text-center">{{$usuario->name}}</p>
    </div>
  </div>
</div>
<!-- hidden-lg show-xs   col-md-2 col-lg-2 imagen_fondo" hidden-xs -->
<div class="container">
  <div class="row">
    <div id="result" style="display: inherit;">
    <div id="result_container" class="result_container ">
     <!-- <img src="img/background.png" width="100%" height="100%"> -->
            <div id="m_castrador" class="col-xs-12 col-sm-3 col-md-3-offset-1 col-lg-2">
                <div id="castrador" class="{{$styleCastrador}}"></div>
                <div id="result_castrador">
                    <p class="text-center">{{$zcastrador}} % </p>
                </div>
            </div>
            <div id="m_desmotivador" class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div id="desmotivador" class="{{$styleDesmotivador}}"></div>
                <div id="result_desmotivador">
                    <p class="text-center">{{$zdesmotivador}} % </p>
                </div>
            </div>
            <div id="m_resistente" class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                <div id="resistente" class="{{$styleCambio}}"></div>
                <div id="result_resistente">
                    <p class="text-center">{{$zcambio}} % </p>
                </div>
            </div>
            <div id="m_negativo" class="col-xs-12 col-md-2 col-sm-2  col-lg-2">
                <div id="negativo" class="{{$styleNegativo}}"></div>
                <div id="result_negativo">
                    <p class="text-center">{{$znegativo}} % </p>
                </div>
            </div>
            <div id="m_chismoso" class="col-xs-12 col-md-2 col-sm-2  col-lg-2">
                <div id="chismoso" class="{{$styleChismoso}}"></div>
                <div id="result_chismoso">
                    <p class="text-center">{{$zchismoso}} % </p>
                </div>
            </div>
    </div>
    {!! Form::open(['url' => '/pdf', 'method' => 'post']) !!}
    <div class="form-group">
    <input type="hidden" name="name" value="{{$usuario->name}}">
    <input type="hidden" name="company" value="{{$usuario->company}}">
    <input type="hidden" name="job" value="{{$usuario->job}}">
    <input type="hidden" name="styleCastrador" value="{{$styleCastrador}}">
    <input type="hidden" name="zcastrador" value="{{$zcastrador}}">
    <input type="hidden" name="styleDesmotivador" value="{{$styleDesmotivador}}">
    <input type="hidden" name="zdesmotivador" value="{{$zdesmotivador}}">
    <input type="hidden" name="styleCambio" value="{{$styleCambio}}">
    <input type="hidden" name="zcambio" value="{{$zcambio}}">
    <input type="hidden" name="styleNegativo" value="{{$styleNegativo}}">
    <input type="hidden" name="znegativo" value="{{$znegativo}} ">
    <input type="hidden" name="styleChismoso" value="{{$styleChismoso}}">
    <input type="hidden" name="zchismoso" value="{{$zchismoso}}">
    </div>
    {!! Form::submit('Generar a PDF', ['class'=>'btn btn-success'])!!}
    {!! Form::close()!!}
    <br>
    </div>
</div>
</div>
