<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>.:HDO- WORKINGDEAD -- {{$name}} :. </title>
    <link rel="stylesheet" href="{{ asset ('css/normalize.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/style_pdf.css')}}">

  </head>
  <body>
    <div class="container">
      <div class="row">
         <center>
           <div class="contenedor_datos">
            <table id="datos">
              <tr>
                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;Nombre: &nbsp;</strong></td><td> {{$name}}</td>
                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;Empresa:&nbsp; </strong></td><td> {{$company}}</td>
                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;Puesto:&nbsp; </strong> </td><td> {{$job}}</td>
              </tr>
            </table>
          </div>
         </center>
    </div>
    <div class="row">
      <center>
        <div class="fondo_grafica">
          <div class="col-md-1">
          </div>
          <div class="col-md-2">
              <div id="castrador" class="{{$styleCastrador}}"></div>
              <div id="result_castrador">
                  <p class="text-center">{{$zcastrador}} % </p>
              </div>
          </div>
          <div class=" col-md-2">
              <div id="desmotivador" class="{{$styleDesmotivador}}"></div>
              <div id="result_desmotivador">
                  <p class="text-center">{{$zdesmotivador}} % </p>
              </div>
          </div>
          <div class="col-md-2">
              <div id="resistente" class="{{$styleCambio}}"></div>
              <div id="result_resistente">
                  <p class="text-center">{{$zcambio}} % </p>
              </div>
          </div>
          <div class="col-md-2">
              <div id="negativo" class="{{$styleNegativo}}"></div>
              <div id="result_negativo">
                  <p class="text-center">{{$znegativo}} % </p>
              </div>
          </div>
          <div class="col-md-2">
              <div id="chismoso" class="{{$styleChismoso}}"></div>
              <div id="result_chismoso">
                  <p class="text-center">{{$zchismoso}} % </p>
              </div>
          </div>
        </div>
      </center>
    </div>

    <!-- hidden-lg show-xs   col-md-2 col-lg-2 imagen_fondo" hidden-xs -->
    <br>
    <footer>
      <div class="container">
       <div class="row">
         <div class="col-xs-12 col-lg-12">
           <br>
            HDO-COMPANY - Derechos Reservados 2017
            <br>
         </div>
       </div>
       <div class="row">
         <div class="col-xs-12 col-lg-12">
           <br>
           <br>
         </div>
       </div>
      </div>
    </footer>
    <script src='{{ asset ('js/jquery.js')}}'></script>
    <script src='{{ asset ('js/bootstrap.min.js')}}'></script>
  </body>
</html>
