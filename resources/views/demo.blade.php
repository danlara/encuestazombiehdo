<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset ('../../favicon.ico')}}">

    <title>.: Reporte PDF| Wolking Dead - Encuenta:.</title>

    <!-- Bootstrap core CSS -->
    {!! Html::style('css/normalize.css') !!}
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/style.css') !!}
    
  </head>
  <body>
    <!-- Fixed navbar -->
    <br>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <p class="text-center"><strong>Nombre: </strong> {{$name}}</p>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <p class="text-center"><strong> Empresa: </strong>{{$company}}</p>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <p class="text-center"><strong> Puesto: </strong> {{$job}}
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div id="result" style="display: inherit;">
        <div id="result_container" class="result_container ">
         <!-- <img src="img/background.png" width="100%" height="100%"> -->
                <div id="m_castrador" class="col-xs-12 col-sm-3 col-md-3-offset-1 col-lg-2">
                    <div id="castrador" class="{{$styleCastrador}}"></div>
                    <div id="result_castrador">
                        <p class="text-center">{{$zcastrador}} % </p>
                    </div>
                </div>
                <div id="m_desmotivador" class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div id="desmotivador" class="{{$styleDesmotivador}}"></div>
                    <div id="result_desmotivador">
                        <p class="text-center">{{$zdesmotivador}} % </p>
                    </div>
                </div>
                <div id="m_resistente" class="col-xs-12 col-md-2 col-sm-2 col-lg-2">
                    <div id="resistente" class="{{$styleCambio}}"></div>
                    <div id="result_resistente">
                        <p class="text-center">{{$zcambio}} % </p>
                    </div>
                </div>
                <div id="m_negativo" class="col-xs-12 col-md-2 col-sm-2  col-lg-2">
                    <div id="negativo" class="{{$styleNegativo}}"></div>
                    <div id="result_negativo">
                        <p class="text-center">{{$znegativo}} % </p>
                    </div>
                </div>
                <div id="m_chismoso" class="col-xs-12 col-md-2 col-sm-2  col-lg-2">
                    <div id="chismoso" class="{{$styleChismoso}}"></div>
                    <div id="result_chismoso">
                        <p class="text-center">{{$zchismoso}} % </p>
                    </div>
                </div>
        </div>
      </div>
    </div>
    </div>
    <br><br><br>
          <!-- /container -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset ('js/jquery.js')}}"></script>
    <script src="{{ asset ('js/bootstrap.min.js')}}"></script>
  <!--  <script src="js/xepOnline.jpPlugin.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset ('js/jquery-3.2.0.min.js')}}"><\/script>')</script> -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <footer>
      <div class="container">
       <div class="row">
         <div class="col-xs-12 col-lg-12">
           <br>
            HDO-COMPANY - Derechos Reservados 2017
            <br>
         </div>
       </div>
       <div class="row">
         <div class="col-xs-12 col-lg-12">
           <br>
           <br>
         </div>
       </div>
      </div>
    </footer>
  </body>
</html>
