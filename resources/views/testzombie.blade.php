@extends('layouts.principal')
@section('title')
  Test Zombie
@endsection
@section('content')
  <div class="container">
    <div id="personal" class="jumbotron">
      {!! Form::open(['url' => '/testend', 'method' => 'post']) !!}
      @foreach ($questions as $question)
      <?php $contador = $contador+1 ?>
      <div class="form-group">
        {{$contador}} .- <label for="{{$question->id}}">{{$question->name}}</label>
        @foreach ($answers as $answer)
          @if ($question->id == $answer->id_questions)
          <div class="radio">
            <label id="resp"><input type="radio" name="{{ $question->id }}" value="{{ $answer->id}}" required="requiered"><p id="resps">{{ $answer->name }}</p></label>
          </div>
          @endif
        @endforeach
      </div>
    @endforeach
      <div class="form-group">
        {{ Form::hidden('active', '1') }}
        {{ $questions->links() }}
      </div>
      <div class="form-group">
        {!! Form::submit('Finalizar Encuesta', ['class'=>'btn btn-primary'])!!}
      </div>
      {!! Form::close()!!}
    </div>
  </div>
@endsection
