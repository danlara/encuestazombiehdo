<!DOCTYPE html>
    <html lang='es'>
    <head>
      <meta charset='utf-8'>
      <meta http-equiv='X-UA-Compatible' content='IE=edge'>
      <meta name='viewport' content='width=device-width, initial-scale=1'>
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name='description' content=''>
      <meta name='author' content=''>
      <link rel='icon' href='{{ asset ('../../favicon.ico')}}'>

      <title>.: Reporte PDF| Wolking Dead - Encuenta:.</title>

      <!-- Bootstrap core CSS -->

    <style>
    th, td {
      padding: 15px;
  }
  table#result{
      width: 768px;
      height: 556px;
      background: url('/img/background_768.png') 0 0 no-repeat;

    }
  footer .container {
    color: white;
    background-image: url('/img/footer_img.png');
    width: 100%;
    height:75px;
  }
  table#result tr td p
  {
    color: white;
  }

    table#resulttbody tr .castrador_0
    {
      position: relative;
      background: url('/img/jefe/Jefe_0.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    table#result tbody tr .castrador_25 {

      background: url('/img/jefe/Jefe_25.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    table#result tbody tr .castrador_50 {

      background: url('/img/jefe/Jefe_50.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    table#result tbody tr .castrador_75 {

      background: url('/img/jefe/Jefe_75.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    table#result tbody tr .castrador_100 {

      background: url('/img/jefe/Jefe_100.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .chismoso_0 {
      position: relative;
      background: url('/img/chismoso/Chismosa_0.png') 0 0 no-repeat;
      background-size: contain;
      width: 10px;

    }

    .chismoso_25 {
      position: relative;
      background: url('/img/chismoso/Chismosa_25.png') 0 0 no-repeat;
      background-size: contain;
      width: 10px;

    }

    .chismoso_50 {
      position: relative;
      background: url('/img/chismoso/Chismosa_50.png') 0 0 no-repeat;
      background-size: contain;
      width: 10px;

    }

    .chismoso_75 {
      position: relative;
      background: url('/img/chismoso/Chismosa_75.png') 0 0 no-repeat;
      background-size: contain;
      width: 10px;

    }

    .chismoso_100 {
      position: relative;
      background: url('/img/chismoso/Chismosa_100.png') 0 0 no-repeat;
      background-size: contain;
      width: 10px;

    }

    .desmotivador_0 {
      position: relative;
      background: url('/img/desmotivador/Desmotivador_0.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .desmotivador_25 {
      position: relative;
      background: url('/img/desmotivador/Desmotivador_25.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .desmotivador_50 {
      position: relative;
      background: url('/img/desmotivador/Desmotivador_50.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .desmotivador_75 {
      position: relative;
      background: url('/img/desmotivador/Desmotivador_75.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .desmotivador_100 {
      position: relative;
      background: url('/img/desmotivador/Desmotivador_100.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .negativo_0 {
      position: relative;
      background: url('/img/negativo/Negativo_0.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .negativo_25 {
      position: relative;
      background: url('/img/negativo/Negativo_25.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .negativo_50 {
      position: relative;
      background: url('/img/negativo/Negativo_50.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .negativo_75 {
      position: relative;
      background: url('/img/negativo/Negativo_75.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .negativo_100 {
      position: relative;
      background: url('/img/negativo/Negativo_100.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .resistente_0 {
      position: relative;
      background: url('/img/resistente/Resistente_0.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .resistente_25 {
      position: relative;
      background: url('/img/resistente/Resistente_25.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .resistente_50 {
      position: relative;
      background: url('/img/resistente/Resistente_50.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .resistente_75 {
      position: relative;
      background: url('/img/resistente/Resistente_75.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }

    .resistente_100 {
      position: relative;
      background: url('/img/resistente/Resistente_100.png') 0 0 no-repeat;
      background-size: contain;
      width: 20px;

    }




   #chismoso{
      color: #fff;
      font-style: 20px;
      width: 20px;
      height:auto;
      margin-top: 55%;
    }
    #desmotivador {
      color: #fff;
      margin-top: 55%;
      font-style: 20px;
      margin-left: 10%;
      width: 87%;
    }
    #castrador{
      margin-left: 17%;
      width: 87%;
      margin-top:55%;
    }

    #resistente{
      color: #fff;
      font-style: 20px;
      margin-left: 10%;
      width: 87%;
      margin-top: 55%;
    }
    #negativo{
      color: #fff;
      font-style: 20px;
      margin-left: 10%;
      width: 87%;
      margin-top: 55%;
    }
    #chismoso {
      margin-left: 6%;
      width: 87%;
    }
    </style>
    </head>
    <body>
      <!-- Fixed navbar -->
      <br>
      <div class='container'>
        <div class='row'>
        <center>
        <table>
          <tr>
            <td><strong> Nombre: </strong></td><td>{{$name}}</td>
            <td><strong> Empresa: </strong></td><td>{{$company}}</td>
            <td><strong> Puesto: </strong></td><td>{{$job}}</td>
          </tr>
        </table>
        </center>
          </div>
        </div>
      <div class='container'>
        <div class='row'>
        <center>
          <table id='result'>
          <tr><td><br><br></td></tr>
          <tr>
          <td><div id='castrador' class='{{$styleCastrador}}'></div><td>
          <td><div id='desmotivador' class='{{$styleDesmotivador}}'></div></td>
          <td><div id='resistente' class='{{$styleCambio}}'></div></td>
          <td><div id='negativo' class='{{$styleNegativo}}'></div></td>
          <td><div id='chismoso' class='{{$styleChismoso}}'></div></td>
          </tr>
          <tr><td><br></td></tr>
          <tr>
          <td><br><p class='text-center'>{{$zcastrador}} % </p></td>
          <td><br><p class='text-center'>{{$zdesmotivador}} % </p></td>
          <td><br><p class='text-center'>{{$zcambio}} % </p></td>
          <td><br><p class='text-center'>{{$znegativo}} % </p></td>
          <td><br><p class='text-center'>{{$zchismoso}} % </p></td>
          </tr>
          </table>
        </center>
      </div>
      </div>
      <br>
            <!-- /container -->
      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src='{{ asset ('js/jquery.js')}}'></script>
      <script src='{{ asset ('js/bootstrap.min.js')}}'></script>
    <!--  <script src='js/xepOnline.jpPlugin.js'></script>
      <script>window.jQuery || document.write('<script src='{ asset ('js/jquery-3.2.0.min.js')}'><\/script>')</script> -->

      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <footer>
        <div class='container'>
         <div class='row'>
           <div class='col-xs-12 col-lg-12'>
             <br>
              HDO-COMPANY - Derechos Reservados 2017
              <br>
           </div>
         </div>
         <div class='row'>
           <div class='col-xs-12 col-lg-12'>
           </div>
         </div>
        </div>
      </footer>
    </body>
  </html>
